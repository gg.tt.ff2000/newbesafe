import 'package:barcode_scan/barcode_scan.dart';
import 'package:corona/component/MySubmitBtn.dart';
import 'package:corona/component/UpNavBar.dart';
import 'package:corona/component/api_data.dart';
import 'package:corona/pages/storge_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity/connectivity.dart';

class ScanPage extends StatefulWidget {
  @override
  _ScanPage createState() => _ScanPage();
}

class _ScanPage extends State<ScanPage> {
  String codeResult = 'أعد المحاولة';
  bool isComplete = false;
  String id;
  String placeSub = '';
  String place;
  bool firstRun = true;
  Future<Data> _scanValue ;
  String sub;
  String checkSub;
  String number;

  getPref()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    id = preferences.getString("id");
    number = preferences.getString("phoneNumber");
    print(id);
    print(number);
  }

  void startScan() async {
    try {
      String codeScanner = await BarcodeScanner.scan();
      var ifConnected = await Connectivity().checkConnectivity();
      if (codeScanner != null) {
        print(codeScanner);
       // print(codeScanner.substring(0,1)as int);
        setState(() {
          codeResult = codeScanner;
          isComplete = true;
          placeSub = codeScanner.substring(9);
          place = placeSub;
          sub = codeScanner.substring(2,8);
          checkSub =  codeScanner.substring(0,1);
          if(ifConnected== ConnectivityResult.wifi || ifConnected == ConnectivityResult.mobile) {
            _scanValue = Data(id: id, number: number, data: sub).checkIn();
            print("done!");
            print('check ''$checkSub');
          }else {
            Storge("scanResult").savePref(sub);
            print("saved");
          }
        });
      } else {
        setState(() {
          codeResult = 'something wrong!!!';
          isComplete = false;
        });
      }

      //Navigator.pop(context, qrCodeResult);
    } catch (e) {
      print(BarcodeScanner.CameraAccessDenied);

      //we can print that user has denied for the permisions
      //BarcodeScanner.UserCanceled;   we can print on the page that user has cancelled
    }
    setState(() {
      firstRun = false;
    });
  }

  @override
  void initState() {
    super.initState();
    startScan();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            UpNavBar(
              title: 'نتيجة ماسح الكود',
              txtColor: Colors.white,
              bgColor: Theme.of(context).primaryColor,
            ),
            SizedBox(
              height: 20,
            ),
            isComplete
                ? Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          width: 138,
                          height: 138,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(100),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    offset: Offset(0, 3),
                                    blurRadius: 6)
                              ]),
                          child: SvgPicture.asset(
                            'assets/icons/done.svg',
                            height: 75,
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          checkSub ==1.toString()
                          ?'أهلاً بك في'
                          :'رافقتك السلامة',
                          style: TextStyle(),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          '$place',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          checkSub==1.toString()
                          ?'تم تسجيل دخولك بنجاح'
                          :'تم تسجيل خروجك بنجاح',
                          style: TextStyle(color: Colors.red),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        MySubmitBtn(
                          text: 'تسجيل دخول آخر',
                          toDo: () {
                            startScan();
                          },
                        ),
                      ],
                    ),
                  )
                : Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 30,
                        ),
                        firstRun
                            ? Container()
                            : Icon(
                                Icons.sms_failed_outlined,
                                color: Colors.red,
                                size: 80.0,
                              ),
                        Text(codeResult, style: TextStyle(fontSize: 20)),
                        SizedBox(
                          height: 30,
                        ),

                        SizedBox(
                          height: 50,
                        ),
                        MySubmitBtn(
                          text: 'حاول من جديد',
                          toDo: () {
                            startScan();
                          },
                        ),
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }

//its quite simple as that you can use try and catch statements too for platform exception

  void showAlert({bool isComplete, String id, String place, String error}) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            content: isComplete
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(20),
                        width: 138,
                        height: 138,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(100),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  offset: Offset(0, 3),
                                  blurRadius: 6)
                            ]),
                        child: SvgPicture.asset(
                          'assets/icons/done.svg',
                          height: 75,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'أهلاً بك في',
                        style: TextStyle(),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 34,
                      ),
                      Text(
                        '$place',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        'تم تسجيل دخولك بنجاح',
                        style: TextStyle(color: Colors.red),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  )
                : Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.sms_failed_outlined,
                        color: Colors.red,
                        size: 50.0,
                      ),
                      Text(error, style: TextStyle(fontSize: 18)),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        error,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
          );
        });
  }
}
