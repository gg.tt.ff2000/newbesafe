import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:connectivity/connectivity.dart';

class Data {
  Data({this.data,this.name,this.number,this.id});
   String data;
   String name ;
   String number;
   String id;



  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      data: json['data'],
      name: json['name'],
      number: json['number'],
      id: json['id'],
    );
  }
  Future<Data> checkIn() async{
    http.Response response =  await http.post(
    'http://www.coronagaza.site/checkin',
    headers: <String, String>{
    'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
    'data':data,
    'name': name,
    'number': number,
    'id' : id,
    }),
    );

    if (response.statusCode == 201) {
      return Data.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to create album.');
    }
  }
}

//    if(type==1){
//    response =  await http.post(
//    'http://www.coronagaza.site/checkin',
//    headers: <String, String>{
//    'Content-Type': 'application/json; charset=UTF-8',
//    },
//    body: jsonEncode(<String, String>{
//    'data': data,
//    }),
//    );
//    }else if(type==2){
//    response =  await http.post(
////    'http://www.coronagaza.site/checkout',
////    headers: <String, String>{
////    'Content-Type': 'application/json; charset=UTF-8',
////    },
////    body: jsonEncode(<String, String>{
////    'data': data,
////    }),
////    );
//    }
